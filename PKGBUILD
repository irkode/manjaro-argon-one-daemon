# Maintainer: irkode <irkode at rikode de>
pkgname=argononed-git
pkgver=r113.7b9593f
pkgrel=2
pkgdesc="Argon One Deamon"
arch=('aarch64')
url="https://gitlab.com/DarkElvenAngel/argononed"
license=('MIT')
groups=()
depends=()
makedepends=('git' 'dtc')
optdepends=()
provides=('argononed')
conflicts=()
replaces=()
backup=('boot/config.txt')
options=()
install=argononed.install
changelog=
source=("argononed::git+https://gitlab.com/DarkElvenAngel/argononed.git")
noextract=()
sha256sums=('SKIP')

pkgver() {
  cd "$srcdir/argononed"
  printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare () {
  cd "$srcdir/argononed"

  # add linker flags from /etc/makepkg.cong
  sed -i "/LFLAGS\s*=/ s/$/ -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now/" makefile
  sed -i "/LFLAGS3\s*=/ s/$/ -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now/" makefile
  sed -i "/-o build\/.*BINAME2/ s/$/ -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now/" makefile

  # adjust path to service
  sed -i "s/^ExecStart=.*/ExecStart=\/usr\/lib\/systemd\/systemd-argononed/"  OS/_common/argononed.service || return 1
  # standard call for shutdown of service
  sed -i "/^ExecStart=/a ExecStop=\/usr\/lib\/systemd\/scripts\/argonone-shutdown"  OS/_common/argononed.service || return 1
}

build() {
  cd $srcdir/argononed 
  if [ -f "makefile.conf" ]; then make mrproper; fi 
  ./configure
  make

  # create module file
  MODULE_FILE="build/argonone.conf"
  [ -f "$MODULE_FILE" ] && rm $MODULE_FILE
  MODULES=`grep "^MODULES" OS/manjaro-arm/i2chelper.sh | cut -d\" -f 2`
  for module in $MODULES
  do
    echo "$module" >> $MODULE_FILE
  done
}

package() {
  cd $srcdir/argononed
  install -D -m 644 LICENSE ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE || return 1
  install -D build/argonone-cli ${pkgdir}/usr/bin/argonone-cli || return 1
  install -D build/argonone.dtbo ${pkgdir}/boot/overlays/argonone.dtbo || return 1
  install -D build/argononed ${pkgdir}/usr/lib/systemd/systemd-argononed || return 1
  install -D build/argonone-shutdown ${pkgdir}/usr/lib/systemd/scripts/argonone-shutdown || return 1
  install -D -m 644 build/argonone.conf ${pkgdir}/etc/modules-load.d/argonone.conf || return 1
  install -D -m 644 OS/_common/argononed.service ${pkgdir}/usr/lib/systemd/system/systemd-argononed.service || return 1
}
