CONFIG_TXT=/boot/config.txt
BASH_COMPLETION_DIR=/usr/share/bash-completion/completions
BASH_COMPLETION_FILE=${BASH_COMPLETION_DIR}/argonone-cli

install_bash_completion() {
    if [ -d "$BASH_COMPLETION_DIR" ]
    then
       echo "Installing bash completions"
       echo 'complete -W " --cooldown --manual --fan --off --auto --fan0 --fan1 --fan2 --temp0 --temp1 --temp2 --hysteresis --commit --reload --decode --verbose --quiet --silent --help --usage --reset " argonone-cli' > $BASH_COMPLETION_FILE
    fi
}
remove_bash_completion() {
    [ -f "$BASH_COMPLETION_FILE" ] && echo "Removing bash completions" && rm $BASH_COMPLETION_FILE 
}

install_overlay() {
    [ -w "$CONFIG_TXT" ] || { echo "ERROR Cannot Write to ${CONFIG_TXT} unable to continue"; exit 1; }

    echo "Installing Overlay $1"
    SYSMODEL=$( awk '{ print $0 }' /proc/device-tree/model | sed 's|Raspberry Pi||;s|Rev.*||;s|Model||;s|Zero|0|;s|Plus|+|;s|B| |;s|A| |;s| ||g' )

    echo -n "Search config.txt for overlay ... "
    grep -i '^dtoverlay=argonone' $CONFIG_TXT 1> /dev/null && { echo "FOUND"; exit 0; } || echo "NOT FOUND"
    echo -n "Insert overlay into ${CONFIG_TXT} ... "
        if [[ `grep -i "^\[pi${SYSMODEL}\]" $CONFIG_TXT` ]]
    then
        sed  -i "/^\[pi${SYSMODEL}\]/a dtoverlay=argonone" $CONFIG_TXT && echo "DONE";
    else
        echo "dtoverlay=argonone" >> $CONFIG_TXT && echo "DONE";
    fi
}

## arg 1:  the new package version
post_install() {
    install_overlay
    install_bash_completion
    systemctl enable systemd-argononed
    systemctl start systemd-argononed
    echo "To activate the argonone overlay a reboot is neccessary"
}

## arg 1:  the new package version
## arg 2:  the old package version
pre_upgrade() {
    systemctl stop systemd-argononed
    remove_bash_completion
}

## arg 1:  the new package version
## arg 2:  the old package version
post_upgrade() {
    install_bash_completion
    systemctl daemon-reload
    systemctl start systemd-argononed
}

## arg 1:  the old package version
pre_remove() {
    systemctl stop systemd-argononed
    systemctl disable systemd-argononed
    remove_bash_completion
}

## arg 1:  the old package version
post_remove() {
    sed -i '/^dtoverlay=argonone/d' $CONFIG_TXT
    echo "To unload the argonone overlay a reboot is neccessary"
}
