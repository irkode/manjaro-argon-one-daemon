# Manjaro ARM Argon One Deamon Package

This is a Manjaro package for the Argon One Deamon written by DarkElvenAngel.

## Purpose

Install Argon One Daemon using standard Manjaro tools and in the standard locations.

## Installation

Sorry late tonight. will continue tomorrow

quick and dirty:

* download the package from the releases page
* sudo pacman -S argononed-git-r113.7b9593f-1-aarch64.pkg.tar.zst
* sudo reboot
* quick test: argonone-cli -m -f 100 -r
  
  should fully speed up the fan
  
* reset: argoneone-cli -a -r


## Copyright

* (c) code is hold by DarkElvenAngel
* (c) packaging stuff by Irkode

## References

Source: https://gitlab.com/DarkElvenAngel/argononed
